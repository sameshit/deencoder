//
//  BaseAudioDecoder.h
//  Decoder
//
//  Created by Oleg on 10.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__BaseAudioDecoder__
#define __Decoder__BaseAudioDecoder__

#include "../../base_decoder/BaseDecoder.h"

namespace DeEncoderLib
{
    class BaseAudioDecoder
    :public BaseDecoder
    {
    public:
        BaseAudioDecoder(CoreObjectLib::CoreObject *core);
        virtual ~BaseAudioDecoder();
        
        virtual bool Open(const LiveFormatLib::AudioHeader &header) = 0;
        virtual bool Close() = 0;
    };
}

#endif /* defined(__Decoder__BaseAudioDecoder__) */
