#include "VorbisDecoder.h"

using namespace CoreObjectLib;
using namespace DeEncoderLib;
using namespace std;
using namespace LiveFormatLib;

VorbisDecoder::VorbisDecoder(CoreObject *core)
:BaseAudioDecoder(core),_is_opened(false)
{

}

VorbisDecoder::~VorbisDecoder()
{
	if (_is_opened)
		Close();
}

bool VorbisDecoder::Open(const AudioHeader &header)
{
	uint8_t *p= (uint8_t*)header.extra_data.c_str();
    int i, hsizes[3];
    unsigned char *headers[3], *extradata = (unsigned char*)header.extra_data.c_str();
	unsigned int offset = 1;

	RETURN_MSG_IF_TRUE(_is_opened,"VorbisDecoder is already opened");
	RETURN_MSG_IF_TRUE(header.extra_data.size() == 0,"Extra data size is zero while opening VorbisDecoder");
	
	vorbis_info_init(&_vi);
    vorbis_comment_init(&_vc);

    if(p[0] == 0 && p[1] == 30) 
	{
        for(i = 0; i < 3; ++i)
		{
			hsizes[i] = Utils::GetBe16(p);
			p+=2;
            headers[i] = p;
            p += hsizes[i];
        }
    } 
	else if(*p == 2) 
	{
        p++;
        for(i=0; i<2; i++) 
		{
            hsizes[i] = 0;
			while((*p == 0xFF) && (offset < header.extra_data.size())) 
			{
                hsizes[i] += 0xFF;
                offset++;
                p++;
            }
			RETURN_MSG_IF_TRUE(offset >= header.extra_data.size() - 1, "Vorbis header sizes are damaged");
            hsizes[i] += *p;
            offset++;
            p++;
        }
		hsizes[2] = header.extra_data.size() - hsizes[0]-hsizes[1]-offset;
        headers[0] = extradata + offset;
        headers[1] = extradata + offset + hsizes[0];
        headers[2] = extradata + offset + hsizes[0] + hsizes[1];
    } 
	else 
	{
		COErr::Set() << "Vorbis initial header len is wrong: "<<*p;
        return false;
    }

    for(i=0; i<3; i++)
	{
        _op.b_o_s= i==0;
        _op.bytes = hsizes[i];
        _op.packet = headers[i];
        RETURN_MSG_IF_TRUE(vorbis_synthesis_headerin(&_vi, &_vc, &_op)<0, i+1 <<" vorbis header is damaged");
    }

    RETURN_MSG_IF_FALSE(vorbis_synthesis_init(&_vd, &_vi) == 0,"vorbis_synthesis_init failed");
    RETURN_MSG_IF_FALSE(vorbis_block_init(&_vd, &_vb) == 0,"vorbis_block_init failed");

	_header = header;
	_is_opened = StartDeEncoder();
	return _is_opened;
}

bool VorbisDecoder::Close()
{
	RETURN_MSG_IF_FALSE(_is_opened,"VorbisDecoder is already closed");

	StopDeEncoder();

	vorbis_info_clear(&_vi);
    vorbis_comment_clear(&_vc);
	return true;
}

inline int Conv(int samples, float **pcm, char *buf, int channels);
void VorbisDecoder::ProcessDeEncode()
{
	Frame frame;
	int16_t *output;
	int samples, total_samples, total_bytes;
	float **pcm;

	if (!_pkt_q->Pop(&_pkt))
		return;
	
	fast_alloc(frame.data,8192*4*2*_header.channels);

	
    output = (int16_t *)frame.data;

	_op.packet = (unsigned char*)_pkt.data.c_str();
	_op.bytes  = _pkt.data.size();

    if(vorbis_synthesis(&_vb, &_op) == 0)
	{
        if (vorbis_synthesis_blockin(&_vd, &_vb) != 0)
		{
			LOG_ERROR("vorbis_syntesis_blockin error");
			return;
		}
	}
	else
	{
		LOG_ERROR("vorbis_synthesis error");
		return;
	}

    total_samples = 0 ;
    total_bytes = 0 ;

    while((samples = vorbis_synthesis_pcmout(&_vd, &pcm)) > 0)
	{
        Conv(samples, pcm, (char*)output + total_bytes, _vi.channels) ;
        total_bytes += samples * 2 * _vi.channels ;
        total_samples += samples ;
        vorbis_synthesis_read(&_vd, samples) ;
    }

	if (total_samples == 0)
	{
		fast_free(frame.data);
		return;
	}
	frame.size = total_samples * _header.channels;
	frame.pts = _pkt.pts;
	if (!_frame_q->Push(frame))
	{
		fast_free(frame.data);
		return;
	}
}

inline const uint16_t Clip(int a)
{
     if (a&(~0xFFFF)) return (-a)>>31;
     else             return a;
}

inline int Conv(int samples, float **pcm, char *buf, int channels)
{
    int i, j;
    ogg_int16_t *ptr, *data = (ogg_int16_t*)buf ;
    float *mono ;

    for(i = 0 ; i < channels ; i++){
        ptr = &data[i];
        mono = pcm[i] ;

        for(j = 0 ; j < samples ; j++) {
            *ptr = Clip(mono[j] * 32767.f);
            ptr += channels;
        }
    }

    return 0 ;
}