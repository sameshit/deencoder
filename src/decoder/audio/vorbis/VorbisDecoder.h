#ifndef VORBISDECODER___H
#define VORBISDECODER___H

#include "../../../../codecs/vorbis/include/vorbisenc.h"
#include "../base_audio_decoder/BaseAudioDecoder.h"

namespace DeEncoderLib
{
	class VorbisDecoder
	:public BaseAudioDecoder
	{
	public:
		VorbisDecoder(CoreObjectLib::CoreObject *core);
		virtual ~VorbisDecoder();
		
		bool Open(const LiveFormatLib::AudioHeader &header);
        bool Close();
    private:
        bool _is_opened;
		LiveFormatLib::AudioHeader	_header;
		LiveFormatLib::Packet		_pkt;
		vorbis_info					_vi;                    
		vorbis_dsp_state			_vd;               
		vorbis_block				_vb;                   
		vorbis_comment				_vc;                 
		ogg_packet					_op;      

		void ProcessDeEncode();
	};
}

#endif