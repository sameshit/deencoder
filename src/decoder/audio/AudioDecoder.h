//
//  AudioDecoder.h
//  Decoder
//
//  Created by Oleg on 10.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__AudioDecoder__
#define __Decoder__AudioDecoder__

#include "base_audio_decoder/BaseAudioDecoder.h"

namespace DeEncoderLib
{
    class AudioDecoder
    {
    private:
        CoreObjectLib::CoreObject *_core;
        BaseAudioDecoder *_decoder;
        bool             _opened;
        
        friend class LiveDecodeTest;
    public:
        AudioDecoder(CoreObjectLib::CoreObject *core);
        virtual ~AudioDecoder();
        
        bool Open(const LiveFormatLib::AudioHeader &header);
        bool Close();
		inline bool PushPacket(const LiveFormatLib::Packet &packet) {return _decoder->PushPacket(packet);}
		inline bool PopFrame(LiveFormatLib::Frame *frame) {return _decoder->PopFrame(frame);}
    };
}

#endif /* defined(__Decoder__AudioDecoder__) */
