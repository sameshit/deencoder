#include "OPUSDecoder.h"

using namespace CoreObjectLib;
using namespace DeEncoderLib;
using namespace std;
using namespace LiveFormatLib;

#define OPUS_ERROR(error) \
	"Error("<<error<<"): "<<opus_strerror(error)

const size_t kOpusMaximumSampleSize = 5760;

OPUSDecoder::OPUSDecoder(CoreObject *core)
:BaseAudioDecoder(core),_is_opened(false)
{

}

OPUSDecoder::~OPUSDecoder()
{
	if (_is_opened)
		Close();
}

bool OPUSDecoder::Open(const AudioHeader &header)
{
	int error;

	RETURN_MSG_IF_TRUE(_is_opened,"OPUSDecoder is already opened");

	_decoder = opus_decoder_create(header.sample_rate, header.channels, &error);
	RETURN_MSG_IF_FALSE(error==OPUS_OK,"Couldn't create opus decoder." << OPUS_ERROR(error));
	_header = header;
	_is_opened = StartDeEncoder();
	return _is_opened;
}

bool OPUSDecoder::Close()
{
	queue<Frame> *frame_q;
	uint8_t *data;

	RETURN_MSG_IF_FALSE(_is_opened,"OPUSDecoder is already closed");

	StopDeEncoder();

	opus_decoder_destroy(_decoder);
	_is_opened = false;
	
	_frame_q->GetQueue(&frame_q);
	while(!frame_q->empty())
	{
		data = frame_q->front().data;
		fast_free(data);
		frame_q->pop();
	}
	return true;
}

void OPUSDecoder::ProcessDeEncode()
{
	Frame frame;

	if (!_pkt_q->Pop(&_pkt))
		return;

	fast_alloc(frame.data,2*kOpusMaximumSampleSize*_header.channels);
	memset(frame.data,0,2*kOpusMaximumSampleSize*_header.channels);
	frame.size = opus_decode(_decoder,(const unsigned char*)_pkt.data.c_str(),
		(opus_int32)_pkt.data.size(),(opus_int16*)frame.data,(int)kOpusMaximumSampleSize,0);
	frame.size *=2*_header.channels;
	frame.pts = _pkt.pts;

	if (frame.size <= 0)
	{
		LOG_ERROR("Couldn't decode OPUS frame. "<<OPUS_ERROR(frame.size));
		fast_free(frame.data);
		return;
	}

	if (!_frame_q->Push(frame))
		fast_free(frame.data);
}