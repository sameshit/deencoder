#ifndef OPUSDECODERR__H
#define OPUSDECODERR__H

#include "../../../../codecs/opus/include/opus.h"
#include "../base_audio_decoder/BaseAudioDecoder.h"

namespace DeEncoderLib
{
	class OPUSDecoder
	:public BaseAudioDecoder
	{
	public:
		OPUSDecoder(CoreObjectLib::CoreObject *core);
		virtual ~OPUSDecoder();
		
		bool Open(const LiveFormatLib::AudioHeader &header);
        bool Close();
    private:
        bool _is_opened;
		LiveFormatLib::AudioHeader _header;
		LiveFormatLib::Packet _pkt;
		OpusDecoder *_decoder;

		void ProcessDeEncode();
	};
}

#endif