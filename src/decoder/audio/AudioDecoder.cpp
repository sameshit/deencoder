//
//  AudioDecoder.cpp
//  Decoder
//
//  Created by Oleg on 10.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "AudioDecoder.h"
#include "aac/AACDecoder.h"
#include "opus/OPUSDecoder.h"
#include "vorbis/VorbisDecoder.h"

using namespace DeEncoderLib;
using namespace CoreObjectLib;
using namespace LiveFormatLib;

AudioDecoder::AudioDecoder(CoreObject *core)
:_decoder(NULL),_core(core),_opened(false)
{

}

AudioDecoder::~AudioDecoder()
{
    if (_decoder != NULL)
        fast_delete(_decoder);
}

bool AudioDecoder::Open(const AudioHeader &header)
{
    if (_opened)
    {
        COErr::Set("Audio decoder is already in opened state");
        return false;
    }
    
    if (_decoder != NULL)
        fast_delete(_decoder);
    
    switch (header.codec)
    {
        case LiveCodecAAC:
            typed_new(AACDecoder,_decoder,_core);
        break;
		case LiveCodecOpus:
			typed_new(OPUSDecoder,_decoder,_core);
		break;
		case LiveCodecVorbis:
			typed_new(VorbisDecoder,_decoder,_core);
		break;
        default:
            COErr::Set("Invalid codec id");
            return false;
        break;
    }
    _opened = _decoder->Open(header);
    if (!_opened)
    {
        fast_delete(_decoder);
        _decoder = NULL;
    }
    return _opened;
}

bool AudioDecoder::Close()
{
    if (!_opened)
    {
        COErr::Set("Audio decoder is not in opened state");
        return false;
    }
    
    _opened = !_decoder->Close();
    
    return !_opened;
}