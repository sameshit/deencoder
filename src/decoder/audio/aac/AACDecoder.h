//
//  AACDecoder.h
//  Decoder
//
//  Created by Oleg on 09.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__AACDecoder__
#define __Decoder__AACDecoder__

#include "../base_audio_decoder/BaseAudioDecoder.h"

#if defined(OS_X)
#import <AudioToolbox/AudioToolbox.h>
#elif defined(OS_WIN)
#include <Mfapi.h>
#include <Objbase.h>
#include <wmcodecdsp.h>
#include <Mftransform.h>
#include <Mferror.h>
#include <Codecapi.h>
#endif

namespace DeEncoderLib
{
    class AACDecoder
    :public BaseAudioDecoder
    {
	public:
        AACDecoder(CoreObjectLib::CoreObject *core);
        virtual ~AACDecoder();
        
        bool Open(const LiveFormatLib::AudioHeader &header);
        bool Close();
    private:
        bool _is_opened;
		LiveFormatLib::AudioHeader _header;
		LiveFormatLib::Packet _pkt;
#if defined(OS_X)
        AudioConverterRef _decoder;
        uint32_t _last_pts;
        int _num_of_decoded_frames;
        AudioStreamBasicDescription _aac_desc,_pcm_desc;
        static OSStatus InputProc(AudioConverterRef converter,UInt32* num_packets,
								  AudioBufferList *buf_list,AudioStreamPacketDescription**  pkt_desc,
								  void* user_data);
        AudioStreamPacketDescription _pkt_desc;
        uint8_t buffer[32*1024];
#elif defined(OS_WIN)
		IMFTransform *_decoder;
		
		bool CreateDecoder();
		void CreateFormat(PHEAACWAVEFORMAT format);
		bool SetInputType();
		bool SetOutputType();
		bool CreateSample(const LiveFormatLib::Packet &pkt,IMFSample **sample);
#endif
        void ProcessDeEncode();
    };
}

#endif /* defined(__Decoder__AACDecoder__) */
