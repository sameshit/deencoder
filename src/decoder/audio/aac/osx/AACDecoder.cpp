//
//  AACDecoder.cpp
//  Decoder
//
//  Created by Oleg on 09.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "AACDecoder.h"

using namespace DecoderLib;
using namespace CoreObjectLib;
using namespace std;
using namespace PeerLib;

#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

AACDecoder::AACDecoder(CoreObject *core)
:BaseAudioDecoder(core),_opened(false)
{
    
}

AACDecoder::~AACDecoder()
{
    Close();
}

bool AACDecoder::Close()
{
    queue<Frame> *frame_q;
    
    if (!_opened)
    {
        COErr::Set("AAC decoder is already closed");
        return false;
    }
    
    _frame_q->StopAll();
    _opened = !StopDecoding();
    if (_opened)
        return false;
    
    _opened = !_decode_thread->OnThread.Deattach(_event_id);
    if (_opened)
    {
        COErr::Set("Couldn't deattach event");
        return false;
    }
    
    _opened = AudioConverterDispose(_decoder) != noErr;
    if (_opened)
    {
        COErr::Set("Couldn't destroy converter object");
        return false;
    }
    
    _frame_q->GetQueue(&frame_q);
    
    while (!frame_q->empty())
    {
        fast_free(frame_q->front().data);
        frame_q->pop();
    }
    
    
    return !_opened;
}

bool AACDecoder::Open(const CoreObjectLib::AudioHeader &header)
{
    OSStatus ret_val;
    string conv_extradata;
    
    if (_opened)
    {
        COErr::Set("AAC decoder is already in opened state");
        return false;
    }
    
    _pkt_desc.mStartOffset = 0;
    
    _aac_desc.mSampleRate        = header.sample_rate;
    _aac_desc.mFormatID          = kAudioFormatMPEG4AAC;
    _aac_desc.mFormatFlags       = kMPEG4Object_AAC_Main;
    _aac_desc.mBitsPerChannel    = 0;
    _aac_desc.mChannelsPerFrame  = header.channels;
    _aac_desc.mBytesPerFrame     = 0;
    _aac_desc.mFramesPerPacket   = 0;
    _aac_desc.mBytesPerPacket    = 0;
    _aac_desc.mReserved          = 0;
    
    _pcm_desc.mSampleRate        = header.sample_rate;
    _pcm_desc.mFormatID          = kAudioFormatLinearPCM;
    _pcm_desc.mFormatFlags       = kLinearPCMFormatFlagIsSignedInteger ;
    _pcm_desc.mBitsPerChannel    = 16;
    _pcm_desc.mChannelsPerFrame  = header.channels;
    _pcm_desc.mBytesPerFrame     = 2 * _pcm_desc.mChannelsPerFrame;
    _pcm_desc.mFramesPerPacket   = 1;
    _pcm_desc.mBytesPerPacket    = _pcm_desc.mBytesPerFrame * _pcm_desc.mFramesPerPacket;
    _pcm_desc.mReserved          = 0;
    
    ret_val = AudioConverterNew(&_aac_desc, &_pcm_desc, &_decoder);
    
    if (ret_val != noErr)
    {
        COErr::Set("Couldn't create audio converter");
        return false;
    }

    conv_extradata = DecoderUtils::MP4ConvertToESDS(header);
    ret_val = AudioConverterSetProperty(_decoder, kAudioConverterDecompressionMagicCookie,(uint32_t) conv_extradata.size(), conv_extradata.c_str());
    if (ret_val != noErr)
    {
        COErr::Set("Couldn't set magic cookie");
        LOG ret_val EL;
        return false;
    }
    
    _event_id = _decode_thread->OnThread.Attach(this,&AACDecoder::ProcessDecode);
    
    _opened = StartDecoding();
    return _opened;
}

void AACDecoder::ProcessDecode()
{
    UInt32 num_packets;
    AudioBufferList buf_list;
    OSStatus status;
    Frame frame;
    
    frame.size = _pcm_desc.mBytesPerPacket*1024;
    fast_alloc(frame.data,frame.size);
    
    num_packets = 1024;
    buf_list.mNumberBuffers = 1;
    buf_list.mBuffers[0].mDataByteSize      = (uint32_t)frame.size;
    buf_list.mBuffers[0].mData              = (void*)frame.data;
    buf_list.mBuffers[0].mNumberChannels    = _aac_desc.mChannelsPerFrame;
    

    status = AudioConverterFillComplexBuffer(_decoder, AACDecoder::InputProc,this,&num_packets,&buf_list,NULL);
    if (status == noErr)
    {
        frame.pts = _last_pts+(uint32_t)_num_of_decoded_frames*(1000*(uint32_t)frame.size/(2 * (uint32_t)_pcm_desc.mSampleRate * (uint32_t)_pcm_desc.mChannelsPerFrame));
        
        ++_num_of_decoded_frames;
        if (!_frame_q->Push(frame))
            fast_free(frame.data);
    }
    else
    {
        fast_free(frame.data);
        if (status != -1)
            LOG "AAC decoding error "<<status EL;
    }
}

OSStatus AACDecoder::InputProc(AudioConverterRef converter, UInt32 *num_packets, AudioBufferList *buf_list, AudioStreamPacketDescription **pkt_desc, void *user_data)
{
    AACDecoder *decoder;
    CoreObject *_core;
    std::string result_buf;    
    
    decoder = (AACDecoder*)user_data;
    _core = decoder->_core;
    
    if (*num_packets != 1)
        *num_packets = 1;
    
    if (!decoder->_pkt_q->Pop(&decoder->_pkt))
        return -1;

    decoder->_num_of_decoded_frames = 0;
    decoder->_last_pts =decoder->_pkt.pts;
    
    *num_packets = 1;
    
    buf_list->mNumberBuffers                        = 1;
    buf_list->mBuffers[0].mData                     = (void*)decoder->_pkt.data.c_str();
    buf_list->mBuffers[0].mDataByteSize             = (uint32_t)decoder->_pkt.data.size();
    buf_list->mBuffers[0].mNumberChannels           = decoder->_pcm_desc.mChannelsPerFrame;

    decoder->_pkt_desc.mDataByteSize    = (uint32_t)decoder->_pkt.data.size();
    decoder->_pkt_desc.mStartOffset     = 0;
    decoder->_pkt_desc.mVariableFramesInPacket = 0;
    
    *pkt_desc = &decoder->_pkt_desc;
    
    return noErr;
}