#include "../AACDecoder.h"

using namespace CoreObjectLib;
using namespace LiveFormatLib;
using namespace DeEncoderLib;
using namespace std;

AACDecoder::AACDecoder(CoreObject *core)
	:BaseAudioDecoder(core),_is_opened(false)
{

}

AACDecoder::~AACDecoder()
{
	if (_is_opened)
		Close();
}

bool AACDecoder::Open(const AudioHeader &header)
{
	RETURN_MSG_IF_TRUE(true,"AAC decoder is not completely implemented for windows");

	RETURN_MSG_IF_TRUE(_is_opened,"AAC decoder is already opened");
	_header = header;

	CoInitialize(NULL);
	RETURN_MSG_IF_FALSE(CreateDecoder(),"Couldn't create AAC decoder");
	RETURN_IF_FALSE(SetInputType());
	RETURN_IF_FALSE(SetOutputType());

	_is_opened = StartDeEncoder();
	return _is_opened;
}

bool AACDecoder::Close()
{
	RETURN_MSG_IF_FALSE(_is_opened,"AAC decoder is already closed");
	RETURN_IF_FALSE(StopDeEncoder());
	_decoder->Release();
	CoUninitialize();
	_is_opened = false;
	return true;
}

void AACDecoder::ProcessDeEncode()
{
	ComUniquePtr<IMFSample> sample;
	HRESULT hr;
	DWORD status,max_len;
	ComUniquePtr<IMFMediaBuffer> buffer;
	uint8_t *buf_data;

	if (!_pkt_q->Pop(&_pkt))
       return;

	if (!CreateSample(_pkt,sample.Receive()))
		FATAL_ERROR("Create sample error: "<<COErr::Get() <<". Aborting...");

process_input:
	hr = _decoder->ProcessInput(0,sample.Get(),0);
	if (hr == MF_E_NOTACCEPTING)
	{ 
		Packet pkt2;
		IMFMediaBuffer *buf2;

		pkt2.data.resize(2000);

		MFT_OUTPUT_DATA_BUFFER output_data_buffer = {0};

		CreateSample(pkt2,&output_data_buffer.pSample);
		output_data_buffer.pSample->GetBufferByIndex(0,&buf2);
		buf2->SetCurrentLength(0);

		while (SUCCEEDED((hr = _decoder->ProcessOutput(0,1,&output_data_buffer,&status))))
		{
			Frame frame;

			if (output_data_buffer.pEvents != NULL)
				output_data_buffer.pEvents->Release();

			hr = output_data_buffer.pSample->GetBufferByIndex(0,buffer.Receive());
			if (FAILED(hr))
				FATAL_ERROR("AAC ProcessDecode: couldn't get buffer by index. "<<PRINTHR(hr));
			hr = buffer->Lock(&buf_data,&max_len,(DWORD*)&frame.size);
			if (FAILED(hr))
				FATAL_ERROR("AAC ProcessDecode: couldn't lock buffer. "<<PRINTHR(hr));

			fast_alloc(frame.data,frame.size);
			memcpy(frame.data,buf_data,frame.size);
			hr = buffer->Unlock();
			if (FAILED(hr))
				FATAL_ERROR("AAC ProcessDecode: couldn't unlock buffer. "<<PRINTHR(hr));

			if (FAILED(hr = output_data_buffer.pSample->GetSampleTime((LONGLONG*)&frame.pts)))
				FATAL_ERROR("AAC ProcessDecode: couldn't get sample time. "<<PRINTHR(hr));
			if (!_frame_q->Push(frame))
			{
				fast_free(frame.data);
				return;
			}
		}
//		if (output_data_buffer.pEvents != NULL)
//			output_data_buffer.pEvents->Release();

		if (hr == MF_E_TRANSFORM_NEED_MORE_INPUT)
			goto process_input;
		else
			FATAL_ERROR("Received unknown error("<<PRINTHR(hr)<<") while processing output.Aborting..");
	}
	else if (FAILED(hr))
		FATAL_ERROR("Received unknonw error("<<PRINTHR(hr)<<") while processin input. Aborting...");
}

bool AACDecoder::CreateSample(const Packet &pkt,IMFSample **sample)
{
	HRESULT hr;
	ComUniquePtr<IMFSample>			usample;
	ComUniquePtr<IMFMediaBuffer>	buffer;
	uint8_t *data;
	DWORD max_len,cur_len;

	hr = MFCreateSample(usample.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create sample. "<<PRINTHR(hr));

	hr = MFCreateMemoryBuffer(pkt.data.size(), buffer.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Coudln't create memory buffer. "<<PRINTHR(hr));

	hr = usample->AddBuffer(buffer.Get());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't add memory buffer to sample. "<<PRINTHR(hr));

	hr = buffer->Lock(&data,&max_len,&cur_len);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't lock buffer. "<<PRINTHR(hr));
	RETURN_MSG_IF_FALSE(cur_len == 0,"Current length is empty");
	RETURN_MSG_IF_FALSE(max_len == pkt.data.size(), "Max len is not equal to pkt length");

	memcpy(data,(void*)pkt.data.c_str(),pkt.data.size());
	hr = buffer->Unlock();
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't unlock buffer. "<<PRINTHR(hr));

	hr = buffer->SetCurrentLength(pkt.data.size());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set current length. "<<PRINTHR(hr));

	hr = usample->SetSampleTime(pkt.pts);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set sample time. "<<PRINTHR(hr));

	hr = usample->SetSampleDuration(pkt.duration);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set duration. "<<PRINTHR(hr));

	*sample = usample.Detach();
	buffer.Detach();
	return true;
}

bool AACDecoder::CreateDecoder()
{
	HRESULT hr = S_OK;
    UINT32 count = 0;
    CLSID *ppCLSIDs = NULL;
    MFT_REGISTER_TYPE_INFO info = { 0 };

    info.guidMajorType = MFMediaType_Audio;
	info.guidSubtype = MFAudioFormat_AAC;

    hr = MFTEnum(   
        MFT_CATEGORY_AUDIO_DECODER,
        0,      // Reserved
        &info,  // Input type
        NULL,   // Output type
        NULL,   // Reserved
        &ppCLSIDs,
        &count
        );

    if (SUCCEEDED(hr) && count == 0)
    {
        hr = MF_E_TOPO_CODEC_NOT_FOUND;
    }

    // Create the first decoder in the list.

    if (SUCCEEDED(hr))
    {
        hr = CoCreateInstance(ppCLSIDs[0], NULL,
            CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&_decoder));
    }

    CoTaskMemFree(ppCLSIDs);
	RETURN_MSG_IF_TRUE(FAILED(hr),""<<PRINTHR(hr))
    return true;
}

bool AACDecoder::SetInputType()
{
	HRESULT hr;
	ComUniquePtr<IMFMediaType> input_type;
	string extra_data;

	hr = MFCreateMediaType(input_type.Receive());
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't create input media type in AAC decoder. "<<PRINTHR(hr));

	hr = input_type->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio);
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set major type video in input type in AAC decoder. "<<PRINTHR(hr));

	hr = input_type->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_AAC);
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set sub type video in input type in AAC decoder. "<<PRINTHR(hr));

	hr = input_type->SetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE,16);
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set bit per sample in AAC decoder. "<<PRINTHR(hr));

	hr = input_type->SetUINT32(MF_MT_AUDIO_NUM_CHANNELS,_header.channels);
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set channels value. "<<PRINTHR(hr));

	hr = input_type->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND,_header.sample_rate);
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set sample rate value. "<<PRINTHR(hr));
	
	struct {
		HEAACWAVEFORMAT format;
		char aac_config_data_pad[256];
	} format;

	CreateFormat(&format.format);

    hr = input_type->SetBlob(
            MF_MT_USER_DATA,
            (UINT8 const *)&format.format.wfInfo.wPayloadType,
			sizeof(HEAACWAVEINFO) - sizeof(WAVEFORMATEX) + _header.extra_data.size()
            );
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set extra data. "<<PRINTHR(hr));

	hr = _decoder->SetInputType(0,input_type.Get(),0);
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set input type. "<<PRINTHR(hr));

	return true;
}

bool AACDecoder::SetOutputType()
{
	bool output_type_found;
	IMFMediaType *output_type;

	output_type_found = false;
	for (auto i = 0; 
		SUCCEEDED(_decoder->GetOutputAvailableType(0, i,&output_type));
		++i) 
	{
		GUID out_subtype = {0};
		HRESULT hr = output_type->GetGUID(MF_MT_SUBTYPE, &out_subtype);
		RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't get subtype in output type in AAC decoder. "<<PRINTHR(hr));

		if (out_subtype == MFAudioFormat_PCM)
		{
			hr = _decoder->SetOutputType(0, output_type, 0);  // No flags
			RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set ouput type in AAC decoder. "<<PRINTHR(hr));
			output_type_found = true;
			break;
		} 
	}
	
	RETURN_MSG_IF_FALSE(output_type_found,"Couldn't find PCM output format in AAC decoder");
	return true;
}

void AACDecoder::CreateFormat(PHEAACWAVEFORMAT format)
{
    PWAVEFORMATEX wf = &format->wfInfo.wfx;
    wf->wFormatTag = WAVE_FORMAT_MPEG_HEAAC;
	wf->nChannels = (WORD)_header.channels;
	wf->nSamplesPerSec = _header.sample_rate;
    wf->nAvgBytesPerSec = 0;
    wf->nBlockAlign = 0;
    wf->wBitsPerSample = 0;
	wf->cbSize = (WORD)(sizeof(format->wfInfo) - sizeof(format->wfInfo.wfx) + _header.extra_data.size());
	PHEAACWAVEINFO hawi = &format->wfInfo;
	hawi->wPayloadType = 0; // The stream contains raw_data_block elements only. 
	hawi->wAudioProfileLevelIndication = 1;// 0x29;
	hawi->wStructType = 0;
	hawi->wReserved1 = 0;
	hawi->dwReserved2 = 0;
	memcpy(format->pbAudioSpecificConfig, _header.extra_data.c_str(), _header.extra_data.size());
}