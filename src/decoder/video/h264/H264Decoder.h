//
//  H264Decoder.h
//  Decoder
//
//  Created by Oleg on 01.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__H264Decoder__
#define __Decoder__H264Decoder__

#include "../base_video_decoder/BaseVideoDecoder.h"

#if defined(OS_X)
#import <VideoDecodeAcceleration/VDADecoder.h>
#elif defined(OS_WIN)
#include <Mfapi.h>
#include <Objbase.h>
#include <wmcodecdsp.h>
#include <Mftransform.h>
#include <Mferror.h>
#include <Codecapi.h>
#endif

namespace DeEncoderLib
{
    class H264Decoder
    :public BaseVideoDecoder
    {
	public:
        H264Decoder(CoreObjectLib::CoreObject *core);
        virtual ~H264Decoder();
        
        bool Open(const LiveFormatLib::VideoHeader &header,LiveFormatLib::PixelFormat *output_format);
        bool Close();
    private:
#if defined(OS_X)
        VDADecoder                   _decoder;
        static void VDACallback(void *data,CFDictionaryRef frameInfo,OSStatus status,uint32_t infoFlags,CVImageBufferRef imageBuffer);
#elif defined(OS_WIN)
		IMFTransform *_decoder;

		bool		GetDecoder();
		bool		SetInputType();
		bool		SetOutputType();
		bool		CreateSample(const LiveFormatLib::Packet &pkt,IMFSample **sample);
#endif
        LiveFormatLib::Packet                     _pkt;
        LiveFormatLib::VideoHeader                _header;
        bool                                      _is_opened;
        
        void ProcessDeEncode();
        void ProcessClose();
    };
}

#endif /* defined(__Decoder__H264Decoder__) */
