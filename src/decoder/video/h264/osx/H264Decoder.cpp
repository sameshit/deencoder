//
//  H264Decoder.cpp
//  Decoder
//
//  Created by Oleg on 01.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "../H264Decoder.h"
#import <Foundation/Foundation.h>

using namespace DecoderLib;
using namespace CoreObjectLib;
using namespace std;
using namespace PeerLib;

NSString* const kDisplayTimeKey = @"display_time";
NSString* const kDecodeTimeKey = @"decode_time";

H264Decoder::H264Decoder(CoreObject *core)
:BaseVideoDecoder(core)
{
}

H264Decoder::~H264Decoder()
{
    std::map<uint32_t,Frame>::iterator it;
    
    if (_is_created)
    {
        VDADecoderDestroy(_decoder);
        for (it = _unordered_frames.begin();
             it != _unordered_frames.end();
             ++it)
            fast_free((*it).second.data);
    }
}

bool H264Decoder::Close()
{
    if (!_is_created)
    {
        COErr::Set("H264 Decoder is not opened");
        return false;
    }
    
    _is_created = !StopDecoding();
    if (_is_created)
        return false;
    
    _is_created = !_decode_thread->OnThread.Deattach(_event_id);
    if (_is_created)
    {
        COErr::Set("Couldn't deattach event");
        return false;
    }
    
    VDADecoderDestroy(_decoder);
    return !_is_created;
}

bool H264Decoder::Open(const CoreObjectLib::VideoHeader &header,PixelFormat *output_format)
{
    NSMutableDictionary *config,*format_info,*io_info;
    NSData *avc_data;
    NSNumber *width,*height,*format,*pf;
    
    _header = header;
    
    width   = [[NSNumber alloc] initWithInt:header.width];
    height  = [[NSNumber alloc] initWithInt:header.height];
    format  = [[NSNumber alloc] initWithInt:'avc1'];
    pf      = [[NSNumber alloc] initWithInt:kCVPixelFormatType_422YpCbCr8];
    
    config = [[NSMutableDictionary alloc] init];
    
    [config setObject:width
           forKey:(NSString*)kVDADecoderConfiguration_Width];
    [config setObject:height
           forKey:(NSString*)kVDADecoderConfiguration_Height];
    [config setObject:format
           forKey:(NSString*)kVDADecoderConfiguration_SourceFormat];
    avc_data = [[NSData alloc] initWithBytes:header.extra_data.c_str() length:header.extra_data.size()];

    [config setObject:avc_data
           forKey:(NSString*)kVDADecoderConfiguration_avcCData];

    format_info = [[NSMutableDictionary alloc] init];
    io_info = [[NSMutableDictionary alloc] init];
    [format_info setObject:pf
                forKey:(NSString*)kCVPixelBufferPixelFormatTypeKey];
    [format_info setObject:io_info
                forKey:(NSString*)kCVPixelBufferIOSurfacePropertiesKey];

    OSStatus status = VDADecoderCreate((CFDictionaryRef)config,
                                   (CFDictionaryRef)format_info, // optional
                                       (VDADecoderOutputCallback*)H264Decoder::VDACallback,
                                   (void *)this,
                                   &_decoder);
    [width release];
    [height release];
    [format release];
    [pf     release];
    [config release];
    [avc_data release];
    [format_info release];
    [io_info release];
    
    _is_created = status == kVDADecoderNoErr;
    if (!_is_created)
    {
        COErr::Set("Coulndn't create decoder with provided parameters",status);
        return false;
    }
    
    _event_id = _decode_thread->OnThread.Attach(this,&H264Decoder::ProcessDecode);
    _is_created=StartDecoding();
    *output_format = PF_VDA;
    return _is_created;
}

void H264Decoder::VDACallback(void *data,CFDictionaryRef ref_frame_info,OSStatus status,uint32_t infoFlags,CVImageBufferRef image_buffer)
{
    H264Decoder *decoder;
    CoreObject *_core;
    Frame frame;
    std::map<uint32_t,Frame>::iterator it;
    NSMutableDictionary *frame_info;
    uint32_t pts,dts;
    NSNumber *ns_pts,*ns_dts;
    CVImageBufferRef copy_buffer;
    
    decoder = (H264Decoder*)data;
    _core   = decoder->_core;
    frame_info = (NSMutableDictionary *)ref_frame_info;
    
    if (status != kVDADecoderNoErr)
    {
        LOG "INTERNAL ERROR: decoding failed "<<status EL;
        return;
    }
    
    copy_buffer = CVBufferRetain(image_buffer);
    frame.data = (uint8_t*)copy_buffer;
    
    ns_pts = [frame_info objectForKey:kDisplayTimeKey];
    pts = [ns_pts unsignedIntValue];
    
    ns_dts = [frame_info objectForKey:kDecodeTimeKey];
    dts = [ns_dts unsignedIntValue];
    
    frame.pts = pts;
    decoder->_unordered_frames.insert(make_pair(pts,frame));
    
    
// Reorder frames by pts. It's using dirty inaccurate method for this
//    , assuming that key-frames appears earlier then each 15 frames.
// Proper handling must calculate POC value from slice header (7.3.3. in h264 specs)
// but it was too hard for Oleg :)
    
    if (pts == dts) 
    {
        for (it = decoder->_unordered_frames.begin();
             it != decoder->_unordered_frames.end() && (*it).second.pts <= pts;
             ++it)
            decoder->_frame_q->Push((*it).second);
        
        decoder->_unordered_frames.erase(decoder->_unordered_frames.begin(), it);
    }
    else if (decoder->_unordered_frames.size() > 15)
    {
        int i;
        for (i = 0,it = decoder->_unordered_frames.begin();
             i < 5;
             ++it,++i)
            decoder->_frame_q->Push((*it).second);
        
        decoder->_unordered_frames.erase(decoder->_unordered_frames.begin(), it);
    }
    
    [frame_info removeAllObjects];
    [frame_info release];
    [ns_pts release];
    [ns_dts release];
}

void H264Decoder::ProcessDecode()
{
    NSData *data;
    NSMutableDictionary *frame_info;
    OSStatus status;
    NSNumber *pts,*dts;
    
    
    if (!_pkt_q->Pop(&_pkt))
        return;
    
    data = [[NSData alloc] initWithBytes:_pkt.data.c_str() length:_pkt.data.size()];
    frame_info = [[NSMutableDictionary alloc] init];
    pts = [[NSNumber alloc] initWithUnsignedInt:_pkt.pts];
    dts = [[NSNumber alloc] initWithUnsignedInt:_pkt.dts];
    
    [frame_info setObject:pts forKey:kDisplayTimeKey];
    [frame_info setObject:dts forKey:kDecodeTimeKey];
    
    status = VDADecoderDecode(_decoder, 0, (CFDataRef)data,
                            (CFDictionaryRef)frame_info);

    
/*    if (status != kVDADecoderNoErr)
        LOG "Couldn't decode packet. Error code: "<<status EL;
    else
        status = VDADecoderFlush(_decoder,kVDADecoderFlush_EmitFrames);*/
    
    if (status != kVDADecoderNoErr)
        LOG "flush error "EL;
    
    [data release];
}