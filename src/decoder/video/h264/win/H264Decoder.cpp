#include "../H264Decoder.h"

using namespace CoreObjectLib;
using namespace DeEncoderLib;
using namespace LiveFormatLib;
using namespace std;

H264Decoder::H264Decoder(CoreObject *core)
	:BaseVideoDecoder(core),_is_opened(false)
{

}

H264Decoder::~H264Decoder()
{
	if (_is_opened)
		Close();
}

bool H264Decoder::Open(const VideoHeader &header,PixelFormat *output_format)
{
	RETURN_MSG_IF_TRUE(true,"H264 decoder is not completely implemented for Windows");

	RETURN_MSG_IF_TRUE(_is_opened,"H264 decoder is already opened");

	_header = header;
	CoInitializeEx(NULL, COINIT_MULTITHREADED);
	
	RETURN_MSG_IF_FALSE(GetDecoder(),"Couldn't create H264 decoder");

	RETURN_IF_FALSE(SetInputType());
	RETURN_IF_FALSE(SetOutputType());	

	*output_format = PF_YUV420p;

	_is_opened = StartDeEncoder();
	return _is_opened;
}

bool H264Decoder::Close()
{
	RETURN_MSG_IF_FALSE(_is_opened,"H264 decoder is already closed");
	RETURN_MSG_IF_FALSE(StopDeEncoder(),"Couldn't stop decoding thread in H264 decoder");
	_decoder->Release();
	CoUninitialize();
	_is_opened = false;
	return true;
}

void H264Decoder::ProcessDeEncode()
{
	ComUniquePtr<IMFSample> sample;
	HRESULT hr;
	MFT_OUTPUT_DATA_BUFFER output_data_buffer;
	DWORD status;

	if (!_pkt_q->Pop(&_pkt))
       return;

	if (!CreateSample(_pkt,sample.Receive()))
		FATAL_ERROR("Create sample error: "<<COErr::Get() <<". Aborting...");

process_input:
	hr = _decoder->ProcessInput(0,sample.Get(),0);
	if (hr == MF_E_NOTACCEPTING)
	{ 
process_output:	
		output_data_buffer.dwStreamID = 0;
		while (SUCCEEDED((hr = _decoder->ProcessOutput(0,1,&output_data_buffer,&status))))
		{
			Frame frame;

			if (output_data_buffer.pEvents != NULL)
				output_data_buffer.pEvents->Release();

			frame.data = (uint8_t*)output_data_buffer.pSample;
			if (FAILED(hr = output_data_buffer.pSample->GetSampleTime((LONGLONG*)&frame.pts)))
				FATAL_ERROR("H264 ProcessDecode: couldn't get sample time. "<<PRINTHR(hr));
			if (!_frame_q->Push(frame))
				return;
		}
/*		if (output_data_buffer.pEvents != NULL)
			output_data_buffer.pEvents->Release();
*/
		if (hr == MF_E_TRANSFORM_STREAM_CHANGE)
			if (SetOutputType())
				goto process_output;
			else
				FATAL_ERROR("Received stream change event and couldn't set valid output type. Error: "<<COErr::Get());
		else if (hr == MF_E_TRANSFORM_NEED_MORE_INPUT)
			goto process_input;
		else
			FATAL_ERROR("Received unknown error("<<PRINTHR(hr)<<") while processing output.Aborting..");
	}
	else if (FAILED(hr))
		FATAL_ERROR("Received unknonw error("<<PRINTHR(hr)<<") while processin input. Aborting...");
}

bool H264Decoder::CreateSample(const Packet &pkt,IMFSample **sample)
{
	HRESULT hr;
	ComUniquePtr<IMFSample>			usample;
	ComUniquePtr<IMFMediaBuffer>	buffer;
	uint8_t *data;
	DWORD max_len,cur_len;

	hr = MFCreateSample(usample.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't create sample. "<<PRINTHR(hr));

	hr = MFCreateMemoryBuffer(pkt.data.size(), buffer.Receive());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Coudln't create memory buffer. "<<PRINTHR(hr));

	hr = usample->AddBuffer(buffer.Get());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't add memory buffer to sample. "<<PRINTHR(hr));

	hr = buffer->Lock(&data,&max_len,&cur_len);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't lock buffer. "<<PRINTHR(hr));
	RETURN_MSG_IF_FALSE(cur_len == 0,"Current length is empty");
	RETURN_MSG_IF_FALSE(max_len == pkt.data.size(), "Max len is not equal to pkt length");

	memcpy(data,(void*)pkt.data.c_str(),pkt.data.size());
	hr = buffer->Unlock();
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't unlock buffer. "<<PRINTHR(hr));

	hr = buffer->SetCurrentLength(pkt.data.size());
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set current length. "<<PRINTHR(hr));

	hr = usample->SetSampleTime(pkt.pts);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set sample time. "<<PRINTHR(hr));

	hr = usample->SetSampleDuration(pkt.duration);
	RETURN_MSG_IF_TRUE(FAILED(hr),"Couldn't set duration. "<<PRINTHR(hr));

	*sample = usample.Detach();
	buffer.Detach();
	return true;
}

bool H264Decoder::GetDecoder()
{
	HRESULT hr = S_OK;
    UINT32 count = 0;

    IMFActivate **ppActivate = NULL;

    MFT_REGISTER_TYPE_INFO info = { MFMediaType_Video, MFVideoFormat_H264 };

    UINT32 unFlags = MFT_ENUM_FLAG_SYNCMFT  | MFT_ENUM_FLAG_LOCALMFT | 
		MFT_ENUM_FLAG_SORTANDFILTER | MFT_ENUM_FLAG_ASYNCMFT | MFT_ENUM_FLAG_HARDWARE | MFT_ENUM_FLAG_TRANSCODE_ONLY;

    hr = MFTEnumEx(MFT_CATEGORY_VIDEO_DECODER,
        unFlags,
        &info,      // Input type
        NULL,       // Output type
        &ppActivate,
        &count);
  
    if (SUCCEEDED(hr) && count == 0)
    {
        hr = MF_E_TOPO_CODEC_NOT_FOUND;
    }

    // Create the first decoder in the list.
    if (SUCCEEDED(hr))
    {
        hr = ppActivate[0]->ActivateObject(IID_PPV_ARGS(&_decoder));
    }

    for (UINT32 i = 0; i < count; i++)
    {
        ppActivate[i]->Release();
    }
    CoTaskMemFree(ppActivate);

	RETURN_MSG_IF_TRUE(FAILED(hr),"GetDecoder error. "<<PRINTHR(hr));
	return true;
}

bool H264Decoder::SetInputType()
{
	HRESULT hr;
	ComUniquePtr<IMFMediaType> input_type;

	hr = MFCreateMediaType(input_type.Receive());
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't create input media type in H264 decoder."<<PRINTHR(hr));

	hr = input_type->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video);
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set major type video in input type in H264 decoder."<<PRINTHR(hr));

	hr = input_type->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_H264);
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set sub type video in input type in H264 decoder."<<PRINTHR(hr));

	hr = input_type->SetUINT32(MF_MT_INTERLACE_MODE,
                             MFVideoInterlace_MixedInterlaceOrProgressive);
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set interlace mode in input type in H264 decoder."<<PRINTHR(hr));
	
	hr = MFSetAttributeSize(input_type.Get(), MF_MT_FRAME_SIZE, _header.width, _header.height);
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set codec width/height in H264 decoder"<<PRINTHR(hr));

	hr = _decoder->SetInputType(0, input_type.Get(), 0);
	RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set input type for H264 decoder"<<PRINTHR(hr));
	
	return true;
}

bool H264Decoder::SetOutputType()
{
	bool output_type_found;
	IMFMediaType *output_type;

	output_type_found = false;
	for (auto i = 0; 
		SUCCEEDED(_decoder->GetOutputAvailableType(0, i,&output_type));
		++i) 
	{
		GUID out_subtype = {0};
		HRESULT hr = output_type->GetGUID(MF_MT_SUBTYPE, &out_subtype);
		RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't get subtype in output type in H264 decoder."<<PRINTHR(hr));

		if (out_subtype == MFVideoFormat_I420)
		{
			hr = _decoder->SetOutputType(0, output_type, 0);  // No flags
			RETURN_MSG_IF_FALSE(SUCCEEDED(hr),"Couldn't set ouput type in H264 decoder."<<PRINTHR(hr));
			output_type_found = true;
			break;
		} 
	}
	
	RETURN_MSG_IF_FALSE(output_type_found,"Couldn't find YUV 420 pixel format in H264 decoder.");
	return true;
}
