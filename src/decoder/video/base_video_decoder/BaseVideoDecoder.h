//
//  BaseVideoDecoder.h
//  Decoder
//
//  Created by Oleg on 10.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__BaseVideoDecoder__
#define __Decoder__BaseVideoDecoder__

#include "../../base_decoder/BaseDecoder.h"

namespace DeEncoderLib
{   
    class BaseVideoDecoder
    :public BaseDecoder
    {
    protected:
        std::map<uint32_t,LiveFormatLib::Frame> _unordered_frames;
    public:
        BaseVideoDecoder(CoreObjectLib::CoreObject *core);
        virtual ~BaseVideoDecoder();
        
        virtual bool Open(const LiveFormatLib::VideoHeader &header,LiveFormatLib::PixelFormat *output_format) = 0;
        virtual bool Close() = 0;
    };
}

#endif /* defined(__Decoder__BaseVideoDecoder__) */
