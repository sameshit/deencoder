//
//  BaseVideoDecoder.cpp
//  Decoder
//
//  Created by Oleg on 10.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BaseVideoDecoder.h"

using namespace DeEncoderLib;
using namespace CoreObjectLib;
using namespace LiveFormatLib;

BaseVideoDecoder::BaseVideoDecoder(CoreObject *core)
:BaseDecoder(core)
{

}

BaseVideoDecoder::~BaseVideoDecoder()
{

}