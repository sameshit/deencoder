//
//  VideoDecoder.cpp
//  Decoder
//
//  Created by Oleg on 10.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "VideoDecoder.h"
#include "h264/H264Decoder.h"
#include "vpx/VPXDecoder.h"

using namespace DeEncoderLib;
using namespace CoreObjectLib;
using namespace LiveFormatLib;

VideoDecoder::VideoDecoder(CoreObject *core)
:_core(core),_decoder(nullptr),_opened(false)
{

}

VideoDecoder::~VideoDecoder()
{
    if (_decoder != NULL)
        fast_delete(_decoder);
}

bool VideoDecoder::Open(const VideoHeader &header, PixelFormat *output_format)
{
    if (_opened)
    {
        COErr::Set("Video decoder is already opened");
        return false;
    }
    
    if (_decoder != NULL)
        fast_delete(_decoder);
    
    switch (header.codec)
    {
        case LiveCodecH264:
            typed_new(H264Decoder,_decoder,_core);
        break;          
		case LiveCodecVP8:
		case LiveCodecVP9:
			typed_new(VPXDecoder,_decoder,_core);
		break;
        default:
            COErr::Set("Invalid codec id");
            return false;
        break;
    }
    
    _opened = _decoder->Open(header,output_format);
    if (!_opened)
    {
        fast_delete(_decoder);
        _decoder = NULL;
    }
    return _opened;
}

bool VideoDecoder::Close()
{
    if (!_opened)
    {
        COErr::Set("Video decoder is not in opened state");
        return false;
    }    
    
    _opened = !_decoder->Close();
    return !_opened;
}