#ifndef VPXDecoderRR__HHH
#define VPXDecoderRR__HHH

#include "../base_video_decoder/BaseVideoDecoder.h"
#define VPX_CODEC_DISABLE_COMPAT 1
#include "../../../../codecs/vpx/include/vpx_decoder.h"
#include "../../../../codecs/vpx/include/vp8dx.h"

namespace DeEncoderLib
{
	class VPXDecoder
	:public BaseVideoDecoder
	{
	public:
		VPXDecoder(CoreObjectLib::CoreObject *core);
		virtual ~VPXDecoder();

		bool Open(const LiveFormatLib::VideoHeader &header,LiveFormatLib::PixelFormat *output_format);
        bool Close();
	private:
		vpx_codec_ctx_t _decoder;
		bool _is_opened;
		LiveFormatLib::Packet _pkt;
		LiveFormatLib::VideoHeader _header;

		void ProcessDeEncode();
		std::string GetError();
	};
}


#endif