#include "VPXDecoder.h"

using namespace CoreObjectLib;
using namespace DeEncoderLib;
using namespace std;
using namespace LiveFormatLib;

VPXDecoder::VPXDecoder(CoreObject *core)
:BaseVideoDecoder(core),_is_opened(false)
{

}

VPXDecoder::~VPXDecoder()
{
	if (_is_opened)
		Close();
}

bool VPXDecoder::Open(const VideoHeader &header, PixelFormat *pf)
{
	RETURN_MSG_IF_TRUE(_is_opened,"VP8 decoder is already opened");

	switch (header.codec)
	{
	case LiveCodecVP8:
		RETURN_MSG_IF_FALSE(vpx_codec_dec_init(&_decoder, vpx_codec_vp8_dx(), NULL, 0) == VPX_CODEC_OK,
			"Coldn't create VP8 decoder. Error: "<<GetError());
	break;
	case LiveCodecVP9:
		RETURN_MSG_IF_FALSE(vpx_codec_dec_init(&_decoder, vpx_codec_vp9_dx(), NULL, 0) == VPX_CODEC_OK,
			"Coldn't create VP9 decoder. Error: "<<GetError());
	break;
	default:
		COErr::Set() << "Invalid codec type("<<header.codec<<") while opening VPX decoder";
		return false;
	break;
	}
	_header = header;
	_is_opened = StartDeEncoder();
	*pf = PF_YUV420p;
	return _is_opened;
}

bool VPXDecoder::Close()
{
	queue<Frame> *frame_q;
	uint8_t *data;

	RETURN_MSG_IF_FALSE(_is_opened,"VP8 decoder is already opened");
	StopDeEncoder();
	RETURN_MSG_IF_FALSE(vpx_codec_destroy(&_decoder) == VPX_CODEC_OK,
		"Couldn't destroy VP8 decoder. Error: "<<GetError());
	_is_opened = false;

	_frame_q->GetQueue(&frame_q);
	while(!frame_q->empty())
	{
		data = frame_q->front().data;
		fast_free(data);
		frame_q->pop();
	}

	return true;
}

void VPXDecoder::ProcessDeEncode()
{
	vpx_image_t *img;
	vpx_codec_iter_t iter = NULL;
	Frame frame;
	uint8_t *pos,*buf;
    uint32_t plane, y;
	size_t plane_sz;
	int i = 0;

	if (!_pkt_q->Pop(&_pkt))
		return;

	ABORT_MSG_IF_FALSE(
	vpx_codec_decode(&_decoder,(const uint8_t*)_pkt.data.c_str(), _pkt.data.size(), NULL, 0)==VPX_CODEC_OK,
	"Couldn't decode VP8 frame: "<<GetError());

    while((img = vpx_codec_get_frame(&_decoder, &iter))) 
	{                   
		frame.size = 3*img->d_h*img->d_w/2;
		fast_alloc(frame.data,frame.size);
		pos = frame.data;
        
		for(plane=0; plane < 3; plane++) 
		{                                
            buf = img->planes[plane]; 
            for(y=0; y < (plane ? (img->d_h + 1) >> 1 : img->d_h); y++) 
			{ 
				plane_sz = plane ? (img->d_w + 1) >> 1 : img->d_w;
				memcpy(pos,buf,plane_sz);
                pos += plane_sz;                                   
				buf += img->stride[plane];                                
			}              
        }   
		frame.pts = _pkt.pts+ i*(1000/24);
		if (!_frame_q->Push(frame))
		{
			fast_free(frame.data);
			return;
		}
		++i;
    }
}

string VPXDecoder::GetError()
{
	stringstream ss;
    const char *detail = vpx_codec_error_detail(&_decoder);    
	
	ss << vpx_codec_error(&_decoder);
    if(detail)
        ss <<", detail: "<<detail;
    return ss.str();        
}

