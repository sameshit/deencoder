//
//  VideoDecoder.h
//  Decoder
//
//  Created by Oleg on 10.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__VideoDecoder__
#define __Decoder__VideoDecoder__

#include "base_video_decoder/BaseVideoDecoder.h"


namespace DeEncoderLib
{
    class VideoDecoder
    {
    public:
        VideoDecoder(CoreObjectLib::CoreObject *core);
        virtual ~VideoDecoder();
        
        bool Open(const LiveFormatLib::VideoHeader &header, LiveFormatLib::PixelFormat *output_format);
        bool Close();
        
		inline bool PushPacket(const LiveFormatLib::Packet &packet) 
					{return _decoder->PushPacket(packet);}
        inline bool PopFrame(LiveFormatLib::Frame *frame)
					{return _decoder->PopFrame(frame);}
	private:
        CoreObjectLib::CoreObject *_core;
        BaseVideoDecoder *_decoder;
        bool            _opened;
    };
}

#endif /* defined(__Decoder__VideoDecoder__) */
