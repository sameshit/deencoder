//
//  BaseDecoder.h
//  Decoder
//
//  Created by Oleg on 04.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__BaseDecoder__
#define __Decoder__BaseDecoder__

#include "../../base/BaseDeEncoder.h"

namespace DeEncoderLib
{
    class BaseDecoder
		:public BaseDeEncoder
	{
    public:
        BaseDecoder(CoreObjectLib::CoreObject *core);
        virtual ~BaseDecoder();
        
		inline bool PushPacket(const LiveFormatLib::Packet &packet) {return _pkt_q->Push(packet);}
		inline bool PopFrame(LiveFormatLib::Frame *frame) {return _frame_q->Pop(frame);}
    };
}

#endif /* defined(__Decoder__BaseDecoder__) */
