#ifndef BaseDeEnCoder___H
#define BaseDeEnCoder___H

#include "../utils/DeEncoderUtils.h"

namespace DeEncoderLib
{
    class BaseDeEncoder
    {
    public:
        BaseDeEncoder(CoreObjectLib::CoreObject *core);
        virtual ~BaseDeEncoder();
	protected:
        CoreObjectLib::CoreObject *_core;
        CoreObjectLib::Thread     *_deencode_thread;
        FrameQueue *_frame_q;
        PacketQueue *_pkt_q;
        bool _is_opened;
        int _event_id;
        
        bool StopDeEncoder();
        bool StartDeEncoder();
	private:
		virtual void ProcessDeEncode() = 0;
    };
}

#endif