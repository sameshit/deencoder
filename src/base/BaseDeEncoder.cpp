#include "BaseDeEncoder.h"

using namespace DeEncoderLib;
using namespace CoreObjectLib;
using namespace std;

BaseDeEncoder::BaseDeEncoder(CoreObject *core)
:_core(core),_is_opened(false)
{
    fast_new(_frame_q, 40);
    fast_new(_pkt_q,60);
    
    _deencode_thread = _core->GetThread();
	_event_id = _deencode_thread->OnThread.Attach(this,&BaseDeEncoder::ProcessDeEncode);
}

BaseDeEncoder::~BaseDeEncoder()
{
    _frame_q->StopAll();
    _pkt_q->StopAll();
    
    _core->ReleaseThread(&_deencode_thread); 
    fast_delete(_frame_q);
    fast_delete(_pkt_q);
}

bool BaseDeEncoder::StartDeEncoder()
{
    RETURN_MSG_IF_TRUE(_is_opened,"BaseDeEncoder is already opened");
    
    _frame_q->Restart();
    _pkt_q->Restart();

    _is_opened = _deencode_thread->Wake();
    return _is_opened;
}

bool BaseDeEncoder::StopDeEncoder()
{
    RETURN_MSG_IF_FALSE(_is_opened,"BaseDeEncoder is already closed");
    
    _frame_q->StopAll();
    _pkt_q->StopAll();
	RETURN_IF_FALSE(_deencode_thread->Sleep());
    _is_opened = false;
    return true;
}