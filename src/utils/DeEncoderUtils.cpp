//
//  DeEnCoderUtils.cpp
//  Decoder
//
//  Created by Oleg on 11.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "DeEncoderUtils.h"

using namespace DeEncoderLib;
using namespace CoreObjectLib;
using namespace std;
using namespace LiveFormatLib;

bool DeEnCoderUtils::H264IsAnnexB(const std::string &annex_b)
{
    const uint8_t *data;
    
    if (annex_b.size() < 4)
        return false;
    data = (const uint8_t*)annex_b.c_str();
    
    return (data[0] == 0x00 && data[1] == 0x00 && data[2] == 0x00 && data[3] == 0x01);
}

bool DeEnCoderUtils::H264MakeHeader(const std::string &annex_b, std::string *avcc)
{
    const uint8_t *pos, *data,*end,*start;
    stringstream ss;
    uint32_t start_code32,*next_data;
    string sps,pps;
    uint8_t be_data[2];
    
    data = (const uint8_t*)annex_b.c_str();
    start = data;
    
    if (annex_b.size() < 4)
    {
        COErr::Set("Invalid size of annex b header");
        return false;
    }
    
    if (!(data[0] == 0x00 && data[1] == 0x00 && data[2] == 0x00 && data[3] == 0x01))
    {
        COErr::Set("provided buffer is not in annex b format");
        return false;
    }
    
    
    Utils::PutBe32((uint8_t*)&start_code32,1);
    
    pos = data + 4;
    next_data = (uint32_t*)pos;
    end = pos;
    
    while (end-start < annex_b.size()-4 && *next_data != start_code32)
    {
        ++end;
        next_data = (uint32_t*)end;
    }
    
    if (end-start >= annex_b.size()-4)
    {
        COErr::Set("Couldn't parse sps data");
        return false;
    }
    
    sps.assign((char*)pos, end-pos);
    
    pos = pos + 4;
    next_data = (uint32_t*)pos;
    end = pos;
    while (end-start < annex_b.size()-4 && ((*next_data != start_code32) ||
                                            (end[0]==0x00 && end[1]==0x00 && end[2]==0x01)) )
    {
        ++end;
        next_data = (uint32_t*)end;
    }
    if (end-start >= annex_b.size()-4)
    {
        COErr::Set("Couldn't parse pps data");
        return false;
    }
    
    pps.assign((char*)pos,end-pos);
    
    ss.put(1);
    ss.put(sps.c_str()[1]);
    ss.put(sps.c_str()[2]);
    ss.put(sps.c_str()[3]);
    
    ss.put(0xFC | 3);
    ss.put(0xE0 | 1);
    
    Utils::PutBe16(be_data, sps.size());
    ss.write((const char*)be_data, 2);
    ss.write(sps.c_str(),sps.size());
    
    Utils::PutBe16(be_data, pps.size());
    ss.write((const char*)be_data, 2);
    ss.write(pps.c_str(),pps.size());
    
    avcc->assign(ss.str());
    return true;
}

bool DeEnCoderUtils::H264MakeNal(const std::string &annex_b, std::string *avcc)
{
    uint32_t start_code,annex_code;
    stringstream ss;
    
    if(annex_b.size() < 4)
    {
        COErr::Set("NAL unit size is less than 4");
        return false;
    }
    
    Utils::PutBe32((uint8_t*)&start_code, 1);
    memcpy((void*)&annex_code,annex_b.c_str(),4);
    
    if (start_code != annex_code)
    {
        COErr::Set("This NAL is not in annex b format");
        return false;
    }
    
    avcc->assign(annex_b);
    Utils::PutBe32((uint8_t*)avcc->c_str(),(uint32_t)annex_b.size()-4);
    return true;
}

void DeEnCoderUtils::MP4PutDescription(uint8_t *pos, int tag, uint32_t size)
{
    int i;
    
    *pos = tag; ++pos;
    for(i = 3; i>0; i--,++pos)
        *pos = (size>>(7*i)) | 0x80;
    *pos = size & 0x7F;
}

string DeEnCoderUtils::MP4ConvertToESDS(const AudioHeader &header)
{
    string ret_buf;
    uint32_t size,decoder_data_size;
    uint8_t *pos;
    
    decoder_data_size = header.extra_data.size()!=0?(uint32_t)header.extra_data.size() + 5: 0;
    size = 3+5+13+decoder_data_size+5+1;
    
    ret_buf.resize(size+5);
    pos = (uint8_t*)ret_buf.c_str();
    
    // ES data
    MP4PutDescription(pos, 0x03,size); pos +=5;
    Utils::PutBe16(pos, 0);         pos +=2;    // track id, always 0
    Utils::PutByte(pos, 0);         pos +=1;    // flags, always 0
    
    // decoder data
    MP4PutDescription(pos, 0x04, 13+decoder_data_size);        pos += 5;
    Utils::PutByte(pos, 0x40);                              pos += 1; // codec type is AAC
    Utils::PutByte(pos, 0x15);                              pos += 1; // stream type is audio
    Utils::PutByte(pos, header.bitstream_size>> (3+16));    pos += 1; // bitstream size packed in 24 bits
    Utils::PutBe16(pos, (header.bitstream_size>>3)&0xFFFF); pos += 2;
    Utils::PutBe32(pos, header.max_bit_rate);               pos += 4; // max bit rate
    Utils::PutBe32(pos, header.bit_rate);                   pos += 4;
    
    if(decoder_data_size != 0)
    {
        MP4PutDescription(pos, 0x05, (uint32_t)header.extra_data.size());  pos += 5;
        memcpy(pos,header.extra_data.c_str(),header.extra_data.size()); pos += header.extra_data.size();
    }
    
    // SL
    MP4PutDescription(pos, 0x06, 1); pos +=5;
    Utils::PutByte(pos, 0x02);
    
    return ret_buf;
}

void DeEnCoderUtils::YUVWritePlane(uint8_t *in_buf, uint32_t linesize, uint32_t w, uint32_t h, uint8_t *out_buf)
{
 	uint8_t *pos = out_buf;
	uint32_t i;
	
	for(i=0;i<h;i++)
	{
		memcpy(pos,in_buf+i*linesize,w);
        pos += w;
	}
}