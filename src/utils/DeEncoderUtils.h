//
//  DeEnCoderUtils.h
//  Decoder
//
//  Created by Oleg on 11.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__DeEnCoderUtils__
#define __Decoder__DeEnCoderUtils__

#include "DeEncoderTypes.h"

namespace DeEncoderLib
{    
    class DeEnCoderUtils
    {
    public:
        static bool         H264IsAnnexB(const std::string &annex_b);
        static bool         H264MakeHeader(const std::string &annex_b,std::string *avcc);
        static bool         H264MakeNal(const std::string &annex_b,std::string *avcc);
        static void         MP4PutDescription(uint8_t *pos,int tag,uint32_t size);
        static std::string  MP4ConvertToESDS(const LiveFormatLib::AudioHeader &header);
        static void         YUVWritePlane(uint8_t *in_buf, uint32_t linesize,
                                          uint32_t w, uint32_t h,uint8_t *out_buf);
    };
}

#endif /* defined(__Decoder__DeEnCoderUtils__) */
