//
//  DecoderTypes.h
//  Decoder
//
//  Created by Oleg on 11.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef Decoder_DecoderTypes_h
#define Decoder_DecoderTypes_h

#include "../../../LiveFormat/src/LiveReader.h"
namespace DeEncoderLib
{
    typedef CoreObjectLib::SharedQueue<LiveFormatLib::Frame> FrameQueue;
    typedef CoreObjectLib::SharedQueue<LiveFormatLib::Packet> PacketQueue;
}

#endif
