#ifndef VIDEOENCODER__HHH
#define VIDEOENCODER__HHH

#include "../../utils/DeEncoderUtils.h"
#include "base_video_encoder/BaseVideoEncoder.h"

namespace DeEncoderLib
{
	class VideoEncoder
	{
	public:
		VideoEncoder(CoreObjectLib::CoreObject *core);
		virtual ~VideoEncoder();

		bool Open(const LiveFormatLib::VideoHeader &header);
		bool Close();

		inline bool PushFrame(const LiveFormatLib::Frame& frame)	{return _encoder->PushFrame(frame);}
		inline bool PopPacket(LiveFormatLib::Packet *pkt)			{return _encoder->PopPacket(pkt);}
	private:
		bool _is_opened;
		CoreObjectLib::CoreObject *_core;
		BaseVideoEncoder *_encoder;
	};
}

#endif