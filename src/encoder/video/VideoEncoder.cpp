#include "VideoEncoder.h"
#include "vpx/VPXEncoder.h"

using namespace DeEncoderLib;
using namespace CoreObjectLib;
using namespace LiveFormatLib;
VideoEncoder::VideoEncoder(CoreObject *core)
:_core(core),_encoder(nullptr),_is_opened(false)
{

}

VideoEncoder::~VideoEncoder()
{
    if (_encoder != nullptr)
        fast_delete(_encoder);
}

bool VideoEncoder::Open(const VideoHeader &header)
{
	RETURN_MSG_IF_TRUE(_is_opened,"VideoEncoder is already opened");
    
    if (_encoder != nullptr)
        fast_delete(_encoder);
    
    switch (header.codec)
    {         
		case LiveCodecVP8:
		case LiveCodecVP9:
			typed_new(VPXEncoder,_encoder,_core);
		break;
        default:
			COErr::Set()<<"VideoEncoder: invalid codec id("<<header.codec<<")";
            return false;
        break;
    }
    
    _is_opened = _encoder->Open(header);
    if (!_is_opened)
    {
        fast_delete(_encoder);
        _encoder = nullptr;
    }
    return _is_opened;
}

bool VideoEncoder::Close()
{
	RETURN_MSG_IF_FALSE (_is_opened, "Video encoder is already closed");
    
    _is_opened = !_encoder->Close();
    return !_is_opened;
}