#ifndef BASEVIDEOCODER__H
#define BASEVIDEOCODER__H

#include "../../base_encoder/BaseEncoder.h"

namespace DeEncoderLib
{
	class BaseVideoEncoder
		:public BaseEncoder
	{
	public:
		BaseVideoEncoder(CoreObjectLib::CoreObject *core);
		virtual ~BaseVideoEncoder();

		virtual bool Open(const LiveFormatLib::VideoHeader &header) = 0;
		virtual bool Close() = 0;
	};
}

#endif