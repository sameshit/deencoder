#include "VPXEncoder.h"

using namespace DeEncoderLib;
using namespace CoreObjectLib;
using namespace std;
using namespace LiveFormatLib;

VPXEncoder::VPXEncoder(CoreObject *core)
	:BaseVideoEncoder(core),_is_opened(false)
{

}

VPXEncoder::~VPXEncoder()
{
	if (_is_opened)
		Close();
}

bool VPXEncoder::Open(const VideoHeader &header)
{
	vpx_codec_enc_cfg_t  cfg;
	vpx_codec_err_t      res;
	vpx_codec_iface_t	 *iface;

	RETURN_MSG_IF_TRUE(_is_opened,"VPXEncoder is already opened");
	RETURN_MSG_IF_FALSE(header.codec == LiveCodecVP8 || header.codec == LiveCodecVP9,
		"VPXEncoder: Invalid codec type("<<header.codec);

	if (header.codec == LiveCodecVP8)
		iface = vpx_codec_vp8_cx();	 
	else
		iface = vpx_codec_vp9_cx();

	res = vpx_codec_enc_config_default(iface, &cfg, 0);  
	RETURN_MSG_IF_TRUE(res,"VPXEncoder: couldn't generate default config: "
		<<vpx_codec_err_to_string(res));

	cfg.rc_target_bitrate = header.bit_rate/1024;                      
	cfg.g_w = header.width;                                                          
	cfg.g_h = header.height;  

	RETURN_MSG_IF_TRUE(vpx_codec_enc_init(&_codec, iface, &cfg, 0),
		"VPXEncoder: couldn't init encoder: "<<GetError());

	_header = header;
	RETURN_IF_FALSE(StartDeEncoder());
	_is_opened = true;
	return true;
}

bool VPXEncoder::Close()
{
	queue<Frame> *frame_q;
	uint8_t *data;

	RETURN_MSG_IF_FALSE(_is_opened,"VPXEncoder is already closed");
	RETURN_IF_FALSE(StopDeEncoder());

	_frame_q->GetQueue(&frame_q);
	while (!frame_q->empty())
	{
		data = frame_q->front().data;
		fast_free(data);
		frame_q->pop();
	}

	RETURN_MSG_IF_TRUE(vpx_codec_destroy(&_codec),
		"VPXEncoder: couldn't destroy encoder: "<<GetError());
	
	_is_opened = false;
	return true;
}

void VPXEncoder::ProcessDeEncode()
{
	Frame							frame;
	vpx_image_t						raw;
	vpx_codec_iter_t				iter;
	const vpx_codec_cx_pkt_t		*vpx_pkt;
	Packet							pkt;
	
	if(!_frame_q->Pop(&frame))
		return;

	if (!vpx_img_wrap(&raw,VPX_IMG_FMT_I420,_header.width,_header.height,1,frame.data))
		FATAL_ERROR("VPXEncoder: Couldn't vpx_img_wrap");

	if(vpx_codec_encode(&_codec, &raw, frame.pts,  
                                1, 0, VPX_DL_REALTIME))
	FATAL_ERROR("VPXEncoder: vpx_codec_encode error: "<<GetError());

	fast_free(frame.data);
	iter = nullptr;
    while( (vpx_pkt = vpx_codec_get_cx_data(&_codec, &iter)) ) 
	{
        switch(vpx_pkt->kind) 
		{
        case VPX_CODEC_CX_FRAME_PKT:                   
			
			pkt.data.assign((char*)vpx_pkt->data.frame.buf,vpx_pkt->data.frame.sz);
			pkt.pts = vpx_pkt->data.frame.pts;
			pkt.duration = vpx_pkt->data.frame.duration;
			pkt.dts = vpx_pkt->data.frame.pts;
            
			if (!_pkt_q->Push(pkt))
				return;
            break;                                                    
        default:
        break;
        }
    }
}

string VPXEncoder::GetError()
{
	stringstream ss;
    const char *detail = vpx_codec_error_detail(&_codec);    
	
	ss << vpx_codec_error(&_codec);
    if(detail)
        ss <<", detail: "<<detail;
    return ss.str();        
}