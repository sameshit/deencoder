#ifndef VPXENCODER__H
#define VPXENCODER__H

#include "../base_video_encoder/BaseVideoEncoder.h"

#define VPX_CODEC_DISABLE_COMPAT 1
#include "../../../../codecs/vpx/include/vpx_encoder.h"
#include "../../../../codecs/vpx/include/vp8cx.h"

namespace DeEncoderLib
{
	class VPXEncoder
		:public BaseVideoEncoder
	{
	public:
		VPXEncoder(CoreObjectLib::CoreObject *core);
		virtual ~VPXEncoder();
		bool Open(const LiveFormatLib::VideoHeader &header);
		bool Close();
	private:
		bool						_is_opened;
		vpx_codec_ctx_t				_codec;
		vpx_image_t					_raw;
		LiveFormatLib::VideoHeader	_header;

		std::string GetError();
		void ProcessDeEncode();
	};
}

#endif