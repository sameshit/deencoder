#ifndef OPUSENCODERR__H
#define OPUSENCODERR__H

#include "../../../../codecs/opus/include/opus.h"
#include "../base_audio_encoder/BaseAudioEncoder.h"

namespace DeEncoderLib
{
	class OPUSEncoder
		:public BaseAudioEncoder
	{
	public:
		OPUSEncoder(CoreObjectLib::CoreObject *core);
		virtual ~OPUSEncoder();
		
		bool Open(const LiveFormatLib::AudioHeader &header);
		bool Close();
	private:
		bool _is_opened;
		uint8_t _packed_data[40*1024]; // max bitrate is 320kpbs
		LiveFormatLib::AudioHeader _header;
		OpusEncoder *_encoder;

		void ProcessDeEncode();
	};
}

#endif