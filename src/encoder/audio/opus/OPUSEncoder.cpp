#include "OPUSEncoder.h"

using namespace DeEncoderLib;
using namespace CoreObjectLib;
using namespace std;
using namespace LiveFormatLib;

#define OPUS_ERROR(error) \
	"Error("<<error<<"): "<<opus_strerror(error)

OPUSEncoder::OPUSEncoder(CoreObject *core)
	:BaseAudioEncoder(core),_is_opened(false)
{

}

OPUSEncoder::~OPUSEncoder()
{
	if (_is_opened)
		Close();
}

bool OPUSEncoder::Open(const AudioHeader &header)
{
	int error;
	RETURN_MSG_IF_TRUE(_is_opened,"OPUSEncoder is already opened");

	_encoder = opus_encoder_create(header.sample_rate,header.channels,OPUS_APPLICATION_AUDIO,&error);
	RETURN_MSG_IF_FALSE(error == OPUS_OK,"Opus encoder error: "<<OPUS_ERROR(error));
	_header = header;
	error = opus_encoder_ctl(_encoder,OPUS_SET_BITRATE(header.bit_rate));
	RETURN_MSG_IF_FALSE(error == OPUS_OK,"Couldn't set encoder bitrate: "<<OPUS_ERROR(error));

	RETURN_IF_FALSE(StartDeEncoder());
	_is_opened = true;
	return true;
}

bool OPUSEncoder::Close()
{
	uint8_t *data;
	queue<Frame> *frame_q;

	RETURN_MSG_IF_FALSE(_is_opened,"OPUSEncoder is already opened");

	RETURN_IF_FALSE(StopDeEncoder());
	_frame_q->GetQueue(&frame_q);
	while (!frame_q->empty())
	{
		data = frame_q->front().data;
		fast_free(data);
		frame_q->pop();
	}

	opus_encoder_destroy(_encoder);
	_is_opened = false;
	return true;
}

void OPUSEncoder::ProcessDeEncode()
{
	Frame frame;
	Packet pkt;
	int frame_size;
	int pkt_size;
	
	if (!_frame_q->Pop(&frame))
		return;

	frame_size = frame.size/2/_header.channels;
	pkt_size = opus_encode(_encoder,(const opus_int16 *)frame.data,frame_size,_packed_data,_header.bit_rate/8);
	if (pkt_size <= 0)
		FATAL_ERROR("OPUSEncoder: encode error: "<<OPUS_ERROR(pkt_size));

	pkt.data.assign((char*)_packed_data,pkt_size);
	pkt.pts = frame.pts;

	fast_free(frame.data);

	if (_pkt_q->Push(pkt))
		return;
}