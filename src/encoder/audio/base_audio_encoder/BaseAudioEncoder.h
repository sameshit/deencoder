#ifndef BASEAUDIOENCODER___H
#define BASEAUDIOENCODER___H

#include "../../base_encoder/BaseEncoder.h"

namespace DeEncoderLib
{
	class BaseAudioEncoder
		:public BaseEncoder
	{
	public:
		BaseAudioEncoder(CoreObjectLib::CoreObject *core);
		virtual ~BaseAudioEncoder();

		virtual bool Open(const LiveFormatLib::AudioHeader &header) = 0;
		virtual bool Close() = 0;
	};
}

#endif