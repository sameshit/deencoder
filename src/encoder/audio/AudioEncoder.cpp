#include "AudioEncoder.h"
#include "opus/OPUSEncoder.h"

using namespace DeEncoderLib;
using namespace CoreObjectLib;
using namespace LiveFormatLib;

AudioEncoder::AudioEncoder(CoreObject *core)
:_core(core),_encoder(nullptr),_is_opened(false)
{

}

AudioEncoder::~AudioEncoder()
{
    if (_encoder != nullptr)
        fast_delete(_encoder);
}

bool AudioEncoder::Open(const AudioHeader &header)
{
	RETURN_MSG_IF_TRUE(_is_opened,"AudioEncoder is already opened");
    
    if (_encoder != nullptr)
        fast_delete(_encoder);
    
    switch (header.codec)
    {         
		case LiveCodecOpus:
			typed_new(OPUSEncoder,_encoder,_core);
		break;
        default:
			COErr::Set()<<"AudioEncoder: invalid codec id("<<header.codec<<")";
            return false;
        break;
    }
    
    _is_opened = _encoder->Open(header);
    if (!_is_opened)
    {
        fast_delete(_encoder);
        _encoder = nullptr;
    }
    return _is_opened;
}

bool AudioEncoder::Close()
{
	RETURN_MSG_IF_FALSE (_is_opened, "Video encoder is already closed");
    
    _is_opened = !_encoder->Close();
    return !_is_opened;
}