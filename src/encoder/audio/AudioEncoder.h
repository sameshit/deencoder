#ifndef AUDIOENCODER__HHH
#define AUDIOENCODER__HHH

#include "../../utils/DeEncoderUtils.h"
#include "base_audio_encoder/BaseAudioEncoder.h"

namespace DeEncoderLib
{
	class AudioEncoder
	{
	public:
		AudioEncoder(CoreObjectLib::CoreObject *core);
		virtual ~AudioEncoder();

		bool Open(const LiveFormatLib::AudioHeader &header);
		bool Close();

		inline bool PushFrame(const LiveFormatLib::Frame& frame)	{return _encoder->PushFrame(frame);}
		inline bool PopPacket(LiveFormatLib::Packet *pkt)			{return _encoder->PopPacket(pkt);}
	private:
		bool _is_opened;
		CoreObjectLib::CoreObject *_core;
		BaseAudioEncoder *_encoder;
	};
}

#endif