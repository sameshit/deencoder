#ifndef BaseEncoder__H
#define BaseEncoder__H

#include "../../base/BaseDeEncoder.h"

namespace DeEncoderLib
{
	class BaseEncoder
		:public BaseDeEncoder
	{
	public:
		BaseEncoder(CoreObjectLib::CoreObject *core);
		virtual ~BaseEncoder();

		inline bool PushFrame(const LiveFormatLib::Frame& frame) {return _frame_q->Push(frame);}
		inline bool PopPacket(LiveFormatLib::Packet* pkt) {return _pkt_q->Pop(pkt);}
	};
}

#endif