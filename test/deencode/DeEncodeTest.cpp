#include "DeEncodeTest.h"

using namespace CoreObjectLib;
using namespace LiveFormatLib;
using namespace std;
using namespace DeEncoderLib;


DeEncodeTest::DeEncodeTest(CoreObject *core)
	:_core(core)
{
	fast_new(_audio_decoder,_core);
	fast_new(_video_decoder,_core);
	fast_new(_audio_encoder,_core);
	fast_new(_video_encoder,_core);
	
	_audio_encode_thread = _core->GetThread();
	_video_encode_thread = _core->GetThread();
	_audio_decode_thread = _core->GetThread();
	_video_decode_thread = _core->GetThread();

	_audio_encode_thread->OnThread.Attach(this,&DeEncodeTest::AudioEncodeThread);
	_audio_decode_thread->OnThread.Attach(this,&DeEncodeTest::AudioDecodeThread);
	_video_decode_thread->OnThread.Attach(this,&DeEncodeTest::VideoDecodeThread);
	_video_encode_thread->OnThread.Attach(this,&DeEncodeTest::VideoEncodeThread);
}

DeEncodeTest::~DeEncodeTest()
{
	ABORT_IF_FALSE(_audio_decoder->Close());
	ABORT_IF_FALSE(_audio_encoder->Close());
	ABORT_IF_FALSE(_video_encoder->Close());
	ABORT_IF_FALSE(_video_decoder->Close());

	_core->ReleaseThread(&_audio_encode_thread);
	_core->ReleaseThread(&_video_encode_thread);
	_core->ReleaseThread(&_audio_decode_thread);
	_core->ReleaseThread(&_video_decode_thread);

	fast_delete(_audio_decoder);
	fast_delete(_video_decoder);
	fast_delete(_audio_encoder);
	fast_delete(_video_encoder);
}

bool DeEncodeTest::Run()
{
	AudioHeader audio_header;
	VideoHeader video_header;
	PixelFormat pf;
	Frame frame;

	audio_header.bit_rate = 96000;
	audio_header.channels = 2;
	audio_header.codec = LiveCodecOpus;
	audio_header.sample_rate = 48000;

	video_header.bit_rate = 300000;
	video_header.codec = LiveCodecVP8;
	video_header.height = 1024;
	video_header.width = 786;

	RETURN_IF_FALSE(_audio_encoder->Open(audio_header));
	RETURN_IF_FALSE(_video_encoder->Open(video_header));
	RETURN_IF_FALSE(_audio_decoder->Open(audio_header));
	RETURN_IF_FALSE(_video_decoder->Open(video_header,&pf));

	RETURN_IF_FALSE(_audio_encode_thread->Wake());
	RETURN_IF_FALSE(_video_encode_thread->Wake());
	RETURN_IF_FALSE(_audio_decode_thread->Wake());
	RETURN_IF_FALSE(_video_decode_thread->Wake());


	for (auto i = 0; i < 10 ; ++i)
	{
		frame.size = video_header.width*video_header.height*3/2;
		fast_alloc(frame.data,frame.size);
		frame.pts = i;

		for (auto j = 0 ; j < frame.size ; ++j)
			frame.data[i] = rand() % 255;

		RETURN_IF_FALSE(_video_encoder->PushFrame(frame));
	}

	for (auto i = 0 ; i < 10; ++i)
	{
		frame.size = 960*4;
		fast_alloc(frame.data,frame.size);
		frame.pts = i;

	for (auto j = 0 ; j < frame.size ; ++j)
		frame.data[i] = rand() % 255;
		
	RETURN_IF_FALSE(_audio_encoder->PushFrame(frame));
	}
	return true;
}

void DeEncodeTest::AudioEncodeThread()
{
	Packet pkt;

	if (!_audio_encoder->PopPacket(&pkt))
		return;

	LOG_INFO("Received audio encoded packet with pts "<<pkt.pts<<", and size "<<pkt.data.size());

	if (!_audio_decoder->PushPacket(pkt))
		FATAL_ERROR("Couldn't push packet to audio decoder");
}

void DeEncodeTest::VideoEncodeThread()
{
	Packet pkt;

	if (!_video_encoder->PopPacket(&pkt))
		return;

	LOG_INFO("Received video encoded packet with pts "<<pkt.pts<<", and size "<<pkt.data.size());

	if (!_video_decoder->PushPacket(pkt))
		FATAL_ERROR("Couldn't push packet to video decoder");
}

void DeEncodeTest::AudioDecodeThread()
{
	Frame frame;

	if (!_audio_decoder->PopFrame(&frame))
		return;

	LOG_INFO("Received audio decoded packet with pts "<<frame.pts<<", and size "<<frame.size);
	fast_free(frame.data);
}

void DeEncodeTest::VideoDecodeThread()
{
	Frame frame;

	if (!_video_decoder->PopFrame(&frame))
		return;

	LOG_INFO("Received video decoded packet with pts "<<frame.pts<<", and size "<<frame.size);
	fast_free(frame.data);
}