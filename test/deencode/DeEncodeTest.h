//
//  LiveDecodeTest.h
//  Decoder
//
//  Created by Oleg on 08.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__LiveDecodeTest__
#define __Decoder__LiveDecodeTest__

#include "../../src/DeEncoder.h"

namespace DeEncoderLib
{
	class DeEncodeTest
	{
	public:
		DeEncodeTest(CoreObjectLib::CoreObject *core);
		virtual ~DeEncodeTest();

		bool Run();
	private:
		CoreObjectLib::CoreObject *_core;
		AudioDecoder *_audio_decoder;
		VideoDecoder *_video_decoder;
		AudioEncoder *_audio_encoder;
		VideoEncoder *_video_encoder;
		CoreObjectLib::Thread *_audio_encode_thread,*_video_encode_thread,
							  *_audio_decode_thread,*_video_decode_thread;
		
		void AudioEncodeThread();
		void VideoEncodeThread();
		void AudioDecodeThread();
		void VideoDecodeThread();
	};
}

#endif /* defined(__Decoder__LiveDecodeTest__) */
