//
//  AvccTest.cpp
//  Decoder
//
//  Created by Oleg on 04.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "AvccTest.h"
#include "../frame_samples.h"

using namespace DeEncoderLib;
using namespace CoreObjectLib;
using namespace std;
using namespace LiveFormatLib;

AvccTest::AvccTest()
{
    _core = new CoreObject;
    fast_new(_decoder,_core);
}

AvccTest::~AvccTest()
{
    fast_delete(_decoder);
    delete _core;
}

bool AvccTest::Run()
{
    VideoHeader header;
	PixelFormat pf;
    Packet pkt;
    FILE *f;
    uint32_t len;
    uint8_t len_data[4],*data;
    
    f = fopen("/Users/void/easy.tv/ffmpeg-latest/test.mp4","r");
    if (f == NULL)
    {
        LOG_ERROR( "can't open file" );
        return false;
    }
    
    if (fseek(f, 51090, SEEK_SET) != 0 )
    {
        LOG_ERROR( "couldn't seek" );
        return false;
    }
    
    header.extra_data.assign((char*)not_optimized,sizeof(not_optimized));
    header.width = 1280;
    header.height = 544;
    if (!_decoder->Open(header,&pf))
    {
        LOG_ERROR( "couldn't create decoder");
        return false;
    }
    
    while (true)
    {
        if (fread(len_data, 4, 1, f) != 1)
        {
            LOG_ERROR( "len read error");
            return false;
        }
        len = Utils::GetBe32(len_data);
        LOG_INFO( "reading "<<len<<" bytes");
        data = new uint8_t[4+len];
        memcpy(data,len_data,4);
        
        if (fread(data+4,len,1,f) != 1)
        {
            LOG_ERROR( "nal read error" );
            return false;
        }
        pkt.data.assign((char*)data,len + 4);
        LOG_INFO( "NAL unit size is "<<pkt.data.size() );
/*        for (int j = 0;  j <str_data.size(); ++j )
        {
            if (j % 4==0)
                cout<<"\t";
            printf(" %x",(unsigned char)str_data[j]);
        }*/
        cout <<endl;
//        str_data.assign((char*)mp4_sample,sizeof(mp4_sample));
        if (!_decoder->PushPacket(pkt))
        {
            LOG_ERROR( "decode error ");
//            return false;
        }
        delete []data;
    }
    
    return true;
}