//
//  AvccTest.h
//  Decoder
//
//  Created by Oleg on 04.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__AvccTest__
#define __Decoder__AvccTest__

#include "../../src/Decoder/video/h264/H264Decoder.h"

namespace DeEncoderLib
{
    class AvccTest
    {
    private:
        CoreObjectLib::CoreObject *_core;
        H264Decoder *_decoder;
    public:
        AvccTest();
        virtual ~AvccTest();
        
        bool Run();
    };
}

#endif /* defined(__Decoder__AvccTest__) */
