//
//  AnnexbTest.cpp
//  Decoder
//
//  Created by Oleg on 04.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "AnnexbTest.h"
#include <iomanip>

using namespace DeEncoderLib;
using namespace CoreObjectLib;
using namespace std;
using namespace LiveFormatLib;

AnnexbTest::AnnexbTest()
{
    _core = new CoreObject;
    fast_new(_decoder,_core);
}

AnnexbTest::~AnnexbTest()
{
    fast_delete(_decoder);
    delete _core;
}

bool AnnexbTest::Run()
{
    uint8_t data[1024],*end,*pos;
    string nal_data;
    uint32_t *code32,start_code;
    VideoHeader header;
	PixelFormat pf;
    Packet pkt;
    
    Utils::PutBe32((uint8_t*)&start_code, 1);
    
    f = fopen("/Users/void/easy.tv/ffmpeg-latest/annexb.h264","r");
    if (f == NULL)
    {
        LOG_ERROR("can't open file");
        return false;
    }
    
    if (fread(data,1024,1,f) != 1)
    {
        LOG_ERROR("Couldn't read header data");
        return false;
    }
    
    pos = data;
    end = pos;
    code32 = (uint32_t *)end;
    
    for (int i = 0 ; i < 2 ; ++i)
    {
        end+=4;
        code32 = (uint32_t*)end;
        while (((*code32 != start_code) ||
                                               (end[0]==0x00 && end[1]==0x00 && end[2]==0x01)) )
        {
            ++end;
            code32 = (uint32_t*)end;
        }
    }
    header.extra_data.assign((char*)data,end-data);
    header.width = 1280;
    header.height = 544;
    LOG_INFO("header parsed");
    
    if (!_decoder->Open(header,&pf))
    {
        LOG_ERROR("Couldn't create h264 decoder: "<<COErr::Get());
        return false;
    }
    
    pos = end;
    end = pos + 4;
   
    while (!feof(f))
    {
        while (end - data < 1024 && ((*code32 != start_code) ||
                                  (end[0]==0x00 && end[1]==0x00 && end[2]==0x01)))
        {
            ++end;
            code32 = (uint32_t*)end;
        }
        if (end-data >= 1024)
        {
            pkt.data.append((char*)pos,end-pos);
            fread(data, 1024, 1, f);
            pos = data;
            end = pos;
            code32 = (uint32_t*)end;            
        }
        else
        {
            pkt.data.append((char*)pos,end-pos);
            
            for (int j = 0;  j <pkt.data.size(); ++j )
            {
                if (j % 4==0)
                    cout<<"\t";
                printf(" %x",(unsigned char)pkt.data[j]);
            }
            cout <<endl;

            if (!_decoder->PushPacket(pkt))
            {
                LOG_ERROR( "decode failed" );
            }
            return true;
        }
    }
 
    
    return true;
}