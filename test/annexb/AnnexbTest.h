//
//  AnnexbTest.h
//  Decoder
//
//  Created by Oleg on 04.02.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Decoder__AnnexbTest__
#define __Decoder__AnnexbTest__

#include "../../src/Decoder/video/h264/H264Decoder.h"

namespace DeEncoderLib
{
    class AnnexbTest
    {
    private:
        CoreObjectLib::CoreObject *_core;
        FILE *f;
        H264Decoder *_decoder;
    public:
        AnnexbTest();
        virtual ~AnnexbTest();
        
        bool Run();
    };
}

#endif /* defined(__Decoder__AnnexbTest__) */
